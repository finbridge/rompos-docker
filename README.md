# RomPOS #

Yet another RomPOS distribution model.

## Installation ##

Install [Docker](https://www.docker.com/), download **docker-compose.yml** file and run from console:


```
docker-compose up -d
```

When download process will be finished you can access RomPOS installation page at **http://localhost:8888**

Use the following data on Database configuration form:

|Field|Value|
|-----|------|
|Host Name|**db**|
|Database Name|**rompos**|
|User Name|**rompos**|
|User Password|**rompos**|


## Screenshots ##

Run Docker under Mac OS

![Screen](screenshot-1.png)

Run Docker under Windows

![Screen](screenshot-2.png)


Run Docker under Linux

![Screen](screenshot-3.png)
